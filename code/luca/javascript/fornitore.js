var side_open=0;
$.getJSON("../php/pietanzeFornitore.php?request=session", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});
function openBar(evt, elem) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(elem).style.display = "block";
  evt.currentTarget.className += " active";

};
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function filterFunction() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  div = document.getElementById("myDropdown");
  a = div.getElementsByTagName("a");
  for (i = 0; i < a.length; i++) {
    txtValue = a[i].textContent || a[i].innerText;
    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      a[i].style.display = "";
    } else {
      a[i].style.display = "none";
    }
  }
}
$(document).ready(function(){
  ready();
  $.getJSON("../php/pietanzeFornitore.php?request=categ", function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      html_code += '<li><label class="container">'+data[i]["categoria"]+'';
      html_code += '<input type="checkbox" id="'+data[i]["categoria"]+'" onclick="clickCateg('+data[i]["categoria"]+')" value="'+data[i]["categoria"]+'">';
      html_code += '<span class="checkmark"></span>';
      html_code += '</li></label>';
      //html_code += '<li><input type="checkbox" id="'+data[i]["categoria"]+'" onclick="clickCateg('+data[i]["categoria"]+')" value="'+data[i]["categoria"]+'">'+data[i]["categoria"]+'</li>';
    }
    html_code+="</ul>";
    $("#mySidenav").append(html_code);
  });
});
function ready(){
  openBar(window.event,'Menu')
  $.getJSON("../php/pietanzeFornitore.php?request=forn", function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      //html_code += '<div id="'+data[i]["id"]+'">';
      html_code +='<li><div id="'+data[i]["id"]+'"><button type="button" id="rimuovibtn'+data[i]["id"]+'" onclick="clickRimuovi('+data[i]["id"]+')" name="rimuovi">Rimuovi</button>';
      html_code +='<label class="'+data[i]["nome"]+'">'+data[i]["nome"]+'</label>';
      html_code +='<label for="value'+data[i]["id"]+'" hidden>lb</label>';
      html_code += '<input type="number" id="value'+data[i]["id"]+'" name="prezzo" min="1.00" max="100.00" step="0.10" value='+data[i]["prezzo"]+'>';
      html_code += '<label >'+data[i]["descrizione"]+'</label> </div></li>';
      //html_code += '</div>';
    }
    html_code += '</ul>'
    html_code += '<button type="button" id="aggiungibtn" class="action-button shadow animate orange" onclick="clickAggiungiBtn()" name="aggiungi">Aggiungi</button>';
    html_code += '<button type="button" id="salvabtn" class="action-button shadow animate orange" onclick="clickSalvaBtn()" name="salva">Salva</button>';
    $("#Menu").html(html_code);
  });
  $.getJSON("../php/recensioneFornitore.php?request=get", function(data) {
    var html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="containerReviews">';
      html_code += '<h1><span>'+data[i]["userUtente"]+'</span> </h1>';
      var stelle = data[i]["stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
    $("#Recensioni").html(html_code);
  });
  window.addEventListener('click', function(e){
    if(side_open){
      if (document.getElementById('mySidenav').contains(e.target)){
      } else{
        if(document.getElementById('side').contains((e.target))){}
        else{
          closeNav();
        }
      }
    }
  });
}
function clickAggiungiBtn(){
  window.location.href = "aggiungiPietanze.html";
}
function clickRimuovi(id){
  $.getJSON("../php/rimuoviPietanze.php?request=rm&id="+id, function(data) {
  });
  $("#"+id).css("display","none");
}
function clickSalvaBtn(){
  $.getJSON("../php/pietanzeFornitore.php?request=user", function(data) {
    session = data[0];
    $.getJSON("../php/modificaPietanze.php?request=user&session="+session+"", function(data) {
      for(var i = 0; i < data.length; i++){
        $.getJSON("../php/modificaPietanze.php?request=price&id="+data[i]["id"]+"&prezzo="+getPrice(data[i]["id"])+"", function(data) {
        });
      }
    });
    /*$("#Menu").html('');
    ready();*/
  });
}
function getPrice(id){
  return $("#value"+id).val();
}
function clickCateg(categoria){
  $.getJSON("../php/pietanzeFornitore.php?request=categ",function(data) {
    var arr = new Array();
    for(var i = 0; i < data.length; i++){
      if($("#"+data[i]["categoria"]).prop("checked")){
        arr.push(data[i]["categoria"]);
      }
    }
    $.getJSON("../php/pietanzeFornitore.php?request=forn",{arr:arr} ,function(data) {
      var html_code = "<ul>";
      for(var i = 0; i < data.length; i++){
        //html_code += '<div id="'+data[i]["id"]+'">';
        html_code +='<li><div id="'+data[i]["id"]+'"><button type="button" id="rimuovibtn'+data[i]["id"]+'" onclick="clickRimuovi('+data[i]["id"]+')" name="rimuovi">Rimuovi</button>';
        html_code +='<label class="'+data[i]["nome"]+'">'+data[i]["nome"]+'</label>'
        html_code += '<input type="number" id="value'+data[i]["id"]+'" name="prezzo" min="1.00" max="100.00" step="0.10" value='+data[i]["prezzo"]+'>';
        html_code += '<label >'+data[i]["descrizione"]+'</label></div></li>';
        //html_code += '</div>';
      }
      html_code += '</ul>'
      html_code += '<button type="button" id="aggiungibtn" class="action-button shadow animate orange" onclick="clickAggiungiBtn()" name="aggiungi">Aggiungi</button>';
      html_code += '<button type="button" id="salvabtn" class="action-button shadow animate orange" onclick="clickSalvaBtn()" name="salva">Salva</button>';
      $("#Menu").html(html_code);
    });
  });
}
function openNav() {
  $("#icons").css("display","none");
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  side_open = 1;
}

function closeNav() {
  side_open = 0;
  $("#icons").css("display","block");
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
function searchbar(){
  if($("#search").val().length > 0){
    $("div#Menu div").each(function(index) {
      if(!$(this).find("label").attr("class").toLowerCase().startsWith($("#search").val().toLowerCase())){
        $(this).css("display","none");
      }else{
        $(this).css("display","block");
      }
    });
  }else{
    $("div#Menu div").each(function(index) {
      $(this).css("display","block");
    });
  }
}
function rmNotification(){
  $.getJSON("../php/notification.php?request=rm", function(data) {
  });
}
