$.getJSON("../php/statoOrdini.php?request=session", function(data) {
  if(data[0] == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});
$(document).ready(function(){
    ready();
    setInterval(ready,15000);
});
function ready(){
  $.getJSON("../php/statoOrdini.php?request=state", function(data) {
    var html_code = "";
    var id=-1;
    var end = 0;
    for(var i = 0; i < data.length; i++){
      if(i>0){
        id=data[i-1]["id"];
      }
      if(id!=data[i]["id"]){
        if(end==1){
          html_code += '<label class="container">Ricevuto';
          html_code += '<input type="checkbox" checked="checked" disabled>';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Evaso';
          html_code += '<input type="checkbox" id="evaso'+id+'" onclick="evadi('+id+')">';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Consegnato';
          html_code += '<input type="checkbox" id="consegnato'+id+'" onclick="consegna('+id+')" disabled >';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '</li>';
          html_code += '</div>' /*modificare perche' quando ci sono piu' ordini esplode" */
          end=0;
        }
        id=data[i]["id"];
        html_code += '<li>';
        html_code += '<div class="'+data[i]["id"]+'">'
        html_code += '<h2>Ordine nr.'+data[i]["id"]+'</h2>';
        html_code += '<h3>'+data[i]["data"]+" | "+data[i]["orario"].substring(0,5)+'</h3>';
        html_code += '<h3>'+data[i]["luogo_consegna"]+'</h3>';
        html_code += '<p>'+data[i]["quantita"]+' '+data[i]["nome"]+'</p>';
        if(data[i+1] == null || data[i+1]["id"]!= data[i]["id"]){
          html_code += '<label class="container">Ricevuto';
          html_code += '<input type="checkbox" checked="checked" disabled>';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Evaso';
          html_code += '<input type="checkbox" id="evaso'+id+'" onclick="evadi('+id+')">';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Consegnato';
          html_code += '<input type="checkbox" id="consegnato'+id+'" onclick="consegna('+id+')" disabled >';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '</li>';
          html_code += '</div>' /*modificare perche' quando ci sono piu' ordini esplode" */
          showState(id);
        }
      }else{
        html_code += '<p>'+data[i]["quantita"]+' '+data[i]["nome"]+'</p>';
        end = 1;
        if(data.length == i+1){
          html_code += '<label class="container">Ricevuto';
          html_code += '<input type="checkbox" checked="checked" disabled>';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Evaso';
          html_code += '<input type="checkbox" id="evaso'+id+'" onclick="evadi('+id+')">';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
          html_code += '<label class="container">Consegnato';
          html_code += '<input type="checkbox" id="consegnato'+id+'" onclick="consegna('+id+')" disabled >';
          html_code += '<span class="checkmark"></span>';
          html_code += '</label>';
        }
        showState(id);
      }
    }
    $("ul").html(html_code);
  });
}
function evadi(id){
  $.getJSON("../php/statoOrdini.php?request=evadi&id="+id, function(data) {
  });
  $("#evaso"+id).attr('disabled',true);
  $("#consegnato"+id).attr('disabled',false);
}
function consegna(id){
  $.getJSON("../php/statoOrdini.php?request=consegna&id="+id, function(data) {
  });
  $("#consegnato"+id).attr('disabled',true);
  $("."+id).remove();
}
function showState(id){
  $.getJSON("../php/statoOrdini.php?request=getstato&id="+id, function(data) {
    if(data[0]["stato"] == 2){
      $("#consegnato"+id).attr('disabled',false);
      $("#evaso"+id).attr('disabled',true);
    }
    if(data[0]["stato"] == 3){
      $("#consegnato"+id).attr('disabled',true);
      $("#evaso"+id).attr('disabled',true);
    }
    if($("#evaso"+id).is(':disabled')){
      $("#evaso"+id).attr('checked',true);
      $("#consegnato"+id).attr('disabled',false);
    }
  });
}
