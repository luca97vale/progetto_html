<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();

	switch ($_GET["request"]) {
    case "getDetails":
      $stmt = $conn->prepare("SELECT * FROM account ac, amministratore a WHERE ac.user = a.user AND a.user = ?");
      $stmt->bind_param("s", $_SESSION["user"]);
      $stmt->execute();
      $result = $stmt->get_result();

      $output = $result->fetch_assoc();

      $stmt->close();
      print json_encode($output);

      break;

    case "email":
      $stmt = $conn->prepare("SELECT `e-mail` FROM account WHERE user != ? AND `e-mail` = ?");
      $stmt->bind_param("ss", $_SESSION["user"], $_GET["email"]);
      $stmt->execute();

			$stmt->store_result();
			$output = $stmt->num_rows;

      $stmt->close();
      print json_encode($output);

      break;

		case "edit":
			$stmt = $conn->prepare("UPDATE account SET nome = ?, cognome = ?, `e-mail` = ?, pwd = ? WHERE user = ?");
			$stmt->bind_param("ssssbs", $_POST["name"], $_POST["surname"], $_POST["email"], $_POST["pwd"], $_SESSION["user"]);

			$stmt->execute();

			$result = $stmt->affected_rows;

			$stmt->close();
			print json_encode($result);

			break;

		case "editImage":
		  $stmt = $conn->prepare("UPDATE account SET image = ? WHERE user = ?");
		  $stmt->bind_param("bs", $_POST["image"], $_SESSION["user"]);
			$stmt->send_long_data(0, $_POST["image"]);

		  $stmt->execute();

		  $result = $stmt->affected_rows;

		  $stmt->close();
		  print json_encode($result);

			break;
  }
}
?>
