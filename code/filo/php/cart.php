<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();

	switch ($_GET["request"]) {
    case "loadCart":
			$value = 0;
      $stmt = $conn->prepare("SELECT o.user_fornitore, p.nome, do.quantita, ps.Prezzo, do.id_ordine, do.id_prodotto, f.nome_negozio FROM ordine o, dettagli_ordine do, prod_specifico ps, prodotto p, fornitore f WHERE o.id = do.id_ordine AND do.id_prodotto = ps.id AND ps.id_prodotto = p.id AND o.user = ? AND o.stato = ? AND o.user_fornitore = f.user");
			$stmt->bind_param("si", $_SESSION["user"], $value);
			$stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;

		case "removeItem":
			$conn = setConnection();
			$stmt = $conn->prepare("DELETE FROM dettagli_ordine WHERE id_ordine = ? AND id_prodotto = ?");
			$stmt->bind_param("ii", $_GET["id_ordine"], $_GET["id_prodotto"]);
			$stmt->execute();

			if ($stmt->affected_rows > 0) {
				$response_array['status'] = 'success';
			} else {
				$response_array['status'] = 'error';
			}

			$stmt->close();
			print json_encode($response_array);

			break;
  }
}
?>
