<?php
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();

	switch ($_GET["request"]) {
		case "vendorsToAccept":
			$stmt = $conn->prepare("SELECT * FROM account a, fornitore f WHERE a.user = f.user AND f.accettato = 0");
			$stmt->execute();

			$result = $stmt->get_result();

			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}

			$stmt->close();
			print json_encode($output);
			break;

		case "vendors":
			$stmt = $conn->prepare("SELECT * FROM account a, fornitore f WHERE a.user = f.user AND f.accettato = 1");
			$stmt->execute();

			$result = $stmt->get_result();

			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}

			$stmt->close();
			print json_encode($output);
			break;

		case "users":
			$stmt = $conn->prepare("SELECT * FROM account a, utente u WHERE a.user = u.user");
			$stmt->execute();

			$result = $stmt->get_result();

			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}

			$stmt->close();
			print json_encode($output);
		  break;
	}
}
?>
