<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();

	switch ($_GET["request"]) {
    case "loadShops":
			$value = 0;
      $stmt = $conn->prepare("SELECT o.*, f.nome_negozio FROM ordine o, fornitore f WHERE o.user_fornitore = f.user AND o.user = ? AND o.stato = ?");
			$stmt->bind_param("si", $_SESSION["user"], $value);
			$stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;

		case "getSlots":
			$stmt = $conn->prepare("SELECT HOUR(o.orario) AS orario, COUNT(*) AS ordini FROM ordine o WHERE o.data = ? AND o.user_fornitore = ? GROUP BY HOUR(o.orario)");
			$stmt->bind_param("ss", $_GET["date"], $_GET["fornitore"]);
			$stmt->execute();
			$result = $stmt->get_result();

			$orders = array();
      while($row = $result->fetch_assoc()) {
        $orders[$row["orario"]] = $row["ordini"];
      }
			$stmt->close();

			$stmt = $conn->prepare("SELECT HOUR(s.ora_inizio) AS ora, s.n_clienti, f.nome_negozio FROM stabilisce s, fornitore f WHERE f.user = s.user AND s.user = ?");
			$stmt->bind_param("s", $_GET["fornitore"]);
			$stmt->execute();
			$result = $stmt->get_result();

			$maxClients = array();
      while($row = $result->fetch_assoc()) {
        $maxClients[$row["ora"]] = array("n_clienti" => $row["n_clienti"], "nome_negozio" => $row["nome_negozio"]);
      }
			$stmt->close();

			$output = array();
			foreach($maxClients as $hour => $vals) {
    		if ((!array_key_exists($hour, $orders) && $vals["n_clienti"] > 0) || $orders[$hour] < $vals["n_clienti"]) {
					$output[] = array("nome_negozio" => $vals["nome_negozio"], "ora" => $hour);
				}
			}

			print json_encode($output);

			break;

    case "isFull":
      $stmt = $conn->prepare("SELECT COUNT(*) AS ordini FROM ordine o WHERE o.data = ? AND o.user_fornitore = ? AND HOUR(o.orario) = ?");
      $stmt->bind_param("sss", $_GET["date"], $_GET["fornitore"], $_GET["ora"]);
      $stmt->execute();
      $result = $stmt->get_result();

      $ordini = $result->fetch_row();
      $stmt->close();

      $stmt = $conn->prepare("SELECT s.n_clienti FROM stabilisce s WHERE s.user = ? AND HOUR(s.ora_inizio) = ?");
      $stmt->bind_param("ss", $_GET["fornitore"], $_GET["ora"]);
      $stmt->execute();
      $result = $stmt->get_result();
      $ordiniMax = $result->fetch_assoc();

      $res = $ordiniMax["n_clienti"] - $ordini[0];

      print json_encode($res);

      break;

		case "insert":
			$stmt = $conn->prepare("UPDATE ordine SET orario = ?, stato = 1, notificato = 0, metodo_pagamento = ?, luogo_consegna = ?, data = ? WHERE user = ? AND user_fornitore = ? AND stato = 0");
			$stmt->bind_param("ssssss", $_POST["time"], $_POST["pagamento"], $_POST["consegna"], $_POST["data"], $_SESSION["user"], $_POST["fornitore"]);
			$stmt->execute();

			$output = $stmt->affected_rows;

			$stmt->close();

			print json_encode($output);
			break;
  }
}
?>
