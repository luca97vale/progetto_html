<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();
  switch ($_GET["request"]) {
    case 'admin':
    $stmt = $conn->prepare("SELECT user FROM amministratore where user = ?");

    $stmt->bind_param("s", $_SESSION["user"]);
    $stmt->execute();

    $result = $stmt->get_result();

    $stmt->close();

    print json_encode($result->num_rows);

    break;

    case 'user':
    $stmt = $conn->prepare("SELECT user FROM utente where user = ?");

    $stmt->bind_param("s", $_SESSION["user"]);
    $stmt->execute();

    $result = $stmt->get_result();

    $stmt->close();

    print json_encode($result->num_rows);

    break;
  }
}
?>
