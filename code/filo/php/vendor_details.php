<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
	$conn = setConnection();

	switch ($_GET["request"]) {
    case "getDetails":
      $stmt = $conn->prepare("SELECT * FROM fornitore f, account a WHERE a.user = f.user AND f.user = ?");
      $stmt->bind_param("s", $_SESSION["user"]);
      $stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;

    case "getMenu":
      $value = 0;
      $stmt = $conn->prepare("SELECT * FROM fornitore f, prodotto p, prod_specifico ps WHERE f.user = ps.user AND p.id = ps.id_prodotto AND f.user = ?");
      $stmt->bind_param("s", $_SESSION["user"]);
      $stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;

    case "getTime":
      $value = 0;
      $stmt = $conn->prepare("SELECT * FROM fornitore f, stabilisce s WHERE f.user = s.user AND f.user = ?");
      $stmt->bind_param("s", $_SESSION["user"]);
      $stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;
  }
}
?>
