<?php
header('Content-Type: application/json');
include "connection.php";

if(isset($_POST["nome"]))
{
	$conn = setConnection();

	$stmt = $conn->prepare("UPDATE fornitore SET accettato = 1 where nome_negozio = ?");
  $stmt->bind_param("s", $_POST["nome"]);
  $stmt->execute();

	if ($stmt->affected_rows > 0) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	$stmt->close();
	print json_encode($response_array);
}
?>
