var notifications = new Map();

function setNotification(stato, notificato, negozio) {
  html_code = "";
  html_code += "<li><article>";

  var notifica = stato - notificato;

  if (notifica == 1) {
    html_code += "Il tuo ordine a "+negozio+" è pronto ed è stato spedito.";
  } else if (notifica == 2) {
    html_code += "Il tuo ordine a "+negozio+" è stato consegnato.";
  }
  html_code += "</article></li>";

  return html_code;
}

function loadNotifications() {
  $.getJSON("/filo/php/notification.php?request=loadNotifications", function(data) {
    var html_code;
    if (data.length > 0 && $("#noti_counter").text().length == 0) {
      var setup = "<ol id=notifications-content></ol>";
      $("#notifications").append(setup);
      for(var i = 0; i < data.length; i++){
        html_code = setNotification(data[i]["stato"], data[i]["notificato"], data[i]["nome_negozio"]);
        notifications.set(data[i]["nome_negozio"], data[i]);
      }
      $("ol#notifications-content").html(html_code);

      if ($(window).width() > 600) {
        $("#noti_counter").text(data.length).animate({ top: '10px', opacity: 1 }, 500);
      } else {
        $("#noti_counter").text(data.length).animate({ top: '5px', opacity: 1 }, 500);
      }

    } else if (data.length > 0 && !$("#noti_counter").is(":hidden") && data.length > $("#noti_counter").text().length) {
      for(var i = 0; i < data.length; i++) {
        if(notifications.has(data[i]["nome_negozio"])) {
          if(notifications.get(data[i]["nome_negozio"])["notificato"] < data[i]["notificato"]) {
            html_code = setNotification(data[i]["stato"], data[i]["notificato"], data[i]["nome_negozio"]);
            $("ol#notifications-content").before(html_code);
            notifications.set(data[i]["nome_negozio"], data[i]);
            $("#noti_counter").text() = $("#noti_counter").text() + 1;
          }
        } else {
          html_code = setNotification(data[i]["stato"], data[i]["notificato"], data[i]["nome_negozio"]);
          $("ol#notifications-content").before(html_code);
          notifications.set(data[i]["nome_negozio"], data[i]);
          $("#noti_counter").text() = $("#noti_counter").text() + 1;
        }
      }
    }
  });
}

$(document).ready(function () {

  $("#notifications").css("display", "none");

  loadNotifications();

  setInterval(loadNotifications, 5000);

  $("#notification").click(function () {

    $("#notifications").fadeToggle("fast", "linear", function () {
      if ($("#notifications").is(":hidden")) {
        $(".fa-bell").removeAttr("id");
        $("ol#notifications-content").remove();
      }
      else {
        if (notifications.size > 0) {
          for (const [key, value] of notifications.entries()) {
            $.post("/filo/php/notification.php?request=updateNotifications", {id:value["id"], stato:value["stato"]}, function(data) {
              if (data > 0) {
                notifications.clear();
              }
            }, "text");
          };
        }
        $(".fa-bell").attr("id", "selected");
      }
    });

    $("#noti_counter").fadeOut("slow");

    return false;
  });

  $(document).click(function () {
    $('#notifications').hide();
    $(".fa-bell").removeAttr("id");
  });
});
