$.getJSON("../php/session.php?request=admin", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});

function searchbar() {
  if($("#search").val().length > 0) {
    if($("input[value='Fornitori']").css("color").toLowerCase() == 'rgb(255, 255, 255)') {
      $("table.vendors tbody tr td:first-child").each(function() {
        if(!$(this).text().toUpperCase().startsWith($("#search").val().toUpperCase())){
          $(this).parent().css("display","none");
        } else {
          $(this).parent().css("display","table-row");
        }
      });
    } else {
      $("table.users tbody tr td:first-child").each(function() {
        if(!$(this).text().toUpperCase().startsWith($("#search").val().toUpperCase())){
          $(this).parent().css("display","none");
        } else {
          $(this).parent().css("display","table-row");
        }
      });
    }
  } else {
    $("tr").each(function() {
      $(this).css("display", "table-row");
    });
  }
}

$(document).ready(function() {

  $('header').on("keyup", "#search", searchbar);
  var vendors = Array();

  $("input[value='Fornitori']").attr("class", "active");

  $("input[value='Fornitori']").click(function() {
    $("div.vendors_page").css("display", "block");
    $("div.users_page").css("display", "none");
    $("input[value='Fornitori']").attr("class", "active");
    $("input[value='Utenti']").removeAttr("class");
    searchbar();
  });

  $("input[value='Utenti']").click(function() {
    $("div.vendors_page").css("display", "none");
    $("div.users_page").css("display", "block");
    $("input[value='Utenti']").attr("class", "active");
    $("input[value='Fornitori']").removeAttr("class");
    searchbar();
  });

  $(".vendors_page .loading").css("display", "block");
  $.getJSON("../php/load_users.php?request=vendors", function(data) {
    for(var i = 0; i < data.length; i++){
      var html_code = "";
      if (data[i]["image"] == null) {
        var image = "../base.jpg";
      } else {
        var image = data[i]["image"];
      }
      vendors[data[i]["nome_negozio"]] = data[i]["user"];
      html_code +='<tr><td class="first">'+data[i]["nome_negozio"]+'</td><td><img src='+image+' alt="Immagine negozio"></td>'+'<td class="last"><form class="acceptRemove"><input class="action-button shadow animate orange acceptRemoveBtn" type="button" value="Rimuovi"></form></td></tr>';
      $("table.vendors tbody").append(html_code);
    }
    $(".vendors_page .loading").css("display", "none");
  });

  $(".users_page .loading").css("display", "block");
  $.getJSON("../php/load_users.php?request=users", function(data) {
    for(var i = 0; i < data.length; i++){
      var html_code = "";
      if (data[i]["image"] == null) {
        var image = "../base.jpg";
      } else {
        var image = data[i]["image"];
      }
      html_code +='<tr><td class="first">'+data[i]["user"]+'</td><td><img src='+image+' alt="Immagine utente"></td>'+'<td class="last"><form class="acceptRemove"><input class="action-button shadow animate orange acceptRemoveBtn" type="button" value="Rimuovi"></form></td></tr>';
      $("table.users tbody").append(html_code);
    }
    $(".users_page .loading").css("display", "none");
  });

  $("tbody.content").on("click", ".acceptRemoveBtn", function() {
    var user;
    var temp = $(this).parent().parent().prev().prev().text();
    if ($("table.users").css("display") == "none") {
      user = vendors[temp];
    } else {
      user = temp;
    }
    $.post("../php/remove_users.php", {user:user}, function() {
      $("td:contains('"+temp+"')").parent().remove();
    })
    .fail(function(data) {
      alert("Something went wrong, contact the admin to solve it");
    }), "text";
  });
});
