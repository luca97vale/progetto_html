$.getJSON("../php/session.php?request=admin", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});

function searchbar() {
  if($("#search").val().length > 0) {
    $("tr td:first-child").each(function() {
      if(!$(this).text().toUpperCase().startsWith($("#search").val().toUpperCase())){
        $(this).parent().css("display","none");
      } else {
        $(this).parent().css("display","table-row");
      }
    });
  } else {
    $("tr").each(function() {
      $(this).css("display", "table-row");
    });
  }
}

$(document).ready(function(){
  $('header').on("keyup", "#search", searchbar);

  $("#loading").css("display", "block");
	$.getJSON("../php/load_users.php?request=vendorsToAccept", function(data) {
		var html_code = "";
		for(var i = 0; i < data.length; i++){
			if (data[i]["image"] == null) {
				var image = "../base.jpg";
			} else {
				var image = data[i]["image"];
			}
			html_code +='<tr><td class="first">'+data[i]["nome_negozio"]+'</td><td><img src='+image+' alt="Immagine negozio"></td>'+'<td class="last"><form class="acceptRemove"><input class="action-button shadow animate orange acceptRemoveBtn" type="button" value="Accetta"></form></td></tr>';
		}

		$("tbody.content").html(html_code);
    $("#loading").css("display", "none");
	});

	$("tbody.content").on("click", ".acceptRemoveBtn", function() {
		var nome = $(this).parent().parent().prev().prev().text();
    $.post("../php/accept_vendor.php", {nome:nome}, function() {
      $("td:contains('"+nome+"')").parent().remove();
    })
		.fail(function(data) {
    	alert("Something went wrong, contact the admin to solve it");
		}), "text";
	});
});
