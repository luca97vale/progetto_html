var user;

$.getJSON("../php/session.php?request=user", function(data) {
  if(data == 0) {
    $.getJSON("../php/session.php?request=admin", function(data) {
      if(data == 0) {
        window.location.href = "../../ale/html/login.html?&err=-1";
      } else {
        user = 0;
      }
    });
  } else {
    user = 1;
  }
});

$(document).ready(function() {

  if (!user) {
    $(".fa-user").parent().attr("href", "../filo/html/edit_admin.html");
    $(".fa-shopping-cart").parent().remove();
    $("#noti_counter").remove().next().remove().next().remove();
    $(".fa-home").parent().attr("href", "../filo/html/initial_Admin_Page.html");
  }

  $("#info").attr("class", "active");

  $("#info").click(function() {
    $("#foods").css("display", "none");
    $("#times").css("display", "none");
    $("#details").css("display", "block");
    $("#info").attr("class", "active");
    $("#menu").removeAttr("class");
    $("#time").removeAttr("class");
  });

  $("#menu").click(function() {
    $("#details").css("display", "none");
    $("#foods").css("display", "block");
    $("#times").css("display", "none");
    $("#info").removeAttr("class");
    $("#time").removeAttr("class");
    $("#menu").attr("class", "active");
  });

  $("#time").click(function() {
    $("#foods").css("display", "none");
    $("#details").css("display", "none");
    $("#times").css("display", "block");
    $("#time").attr("class", "active");
    $("#menu").removeAttr("class");
    $("#info").removeAttr("class");
  });

  $.getJSON("../php/vendor_details.php?request=getDetails", function(data) {
    if (data["image"] == null) {
      var image = "../base.jpg";
    } else {
      var image = data["image"];
    }
    $("#details img").attr("src", image);
    $("h1").text(data["nome_negozio"]);
    $("input#name").val(data["nome"]);
    $("input#surname").val(data["cognome"]);
    $("input#p_iva").val(data["p_iva"]);
  });

  $.getJSON("../php/vendor_details.php?request=getMenu", function(data) {
    
  });

  $.getJSON("../php/vendor_details.php?request=getTime", function(data) {
  });
});
