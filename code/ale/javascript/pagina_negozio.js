$.getJSON("../php/pagina_negozio.php?request=session", function(data) {
  if(data[0] == 0){
    window.location.replace("../../ale/html/login.html");
  }
});

function searchbar(){
  if($("#search").val().length > 0){
    $("div#Menu div").each(function(index) {
      if(!$(this).find("label").attr("class").toLowerCase().startsWith($("#search").val().toLowerCase())){
        $(this).css("display","none");
      } else {
        $(this).css("display","block");
      }
    });
  }else{
    $("div#Menu div").each(function(index) {
      $(this).css("display","block");
    });
  }
}

function openBar(evt, elem) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(elem).style.display = "block";
  evt.currentTarget.className += " active";
};

$(document).ready(function(){
  ready();
  $('header').on("keyup", "#search", searchbar);
});

function ready(){
  openBar(window.event,'Menu');
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=forn&negozio="+param, function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      html_code += '<div id="'+data[i]["id"]+'">';
      html_code += '<li><button type="button" onclick="send('+data[i]["id"]+')"> Aggiungi al carrello </button>';
      html_code += '<label class="'+data[i]["nome"]+'">'+data[i]["nome"]+'</label>';
      html_code += '<input type="number" id="value'+data[i]["id"]+'" name="prezzo" value='+data[i]["prezzo"]+' disabled>';
      html_code += '<label >'+data[i]["descrizione"]+'</label>';
      html_code += '<input type="number" min="0" step="1" name="'+data[i]["id"]+'" id="'+data[i]["id"]+'" placeholder="Inserisci quantità"> </input> </li>';
      html_code += '</div>';
    }
    html_code += '</ul>'
    // var html_code = "";
    // for(var i = 0; i < data.length; i++){
    //   html_code +='<label>'+data[i]["nome"]+'</label>'
    //   html_code += '<li><label >'+data[i]["descrizione"]+'</label>';
    //   html_code += '<input type="number" disabled id="value'+data[i]["id"]+'" name="prezzo" min="1.00" max="100.00" step="0.10" value='+data[i]["prezzo"]+'>€ <button type="button" onclick="send('+data[i]["id"]+
		// 			')"> Aggiungi al carrello </button>  <input type="text" name="'+data[i]["id"]+'" id="'+data[i]["id"]+'" placeholder="Inserisci quantità"> </input> </li> </ul>';
    // }
    $("#Menu").html(html_code);
  });
  //VIASUALIZZARE E AGGIUNGERE RECENSIONI
  // openBar(window.event,'Recensioni');
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=get&param="+param, function(data) {
    var html_code = "";
    html_code += 'Inserisci la tua recensione!';
  	html_code += '<label> Inserisci punteggio </label> <input id="stelle" type="number" placeholder="Inserisci punteggio da 0 a 5"> </input> <br>'+
  	'<label> Inserisci recensione </label> <input id="recens" type="text" placeholder="Inserisci recensione"> </input> <br>'+
  	'<button type="button" onclick="send_rec"> Aggiungi una recensione </button>';
    $("#Recensioni").html(html_code);

    html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="containerReviews">';
      html_code += '<h1><span>'+data[i]["userUtente"]+'</span> </h1>';
      var stelle = data[i]["Stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
	  $("#Recensioni").append(html_code);
  });


  //Info
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=info&param="+param, function(data) {
    $("h1#nome_negozio").text(data["nome_negozio"]);

    var html_code = "";
    html_code += '<label for="name">Nome</label>' +
      '<input type="text" class="input" id="name" value="'+data["nome"]+'"/>' +
      '<label for="surname">Cognome</label>' +
      '<input type="text" class="input" id="surname" value="'+data["cognome"]+'"/>' +
      '<label for="email">Email</label>' +
      '<input type="email" class="input" id="email" value="'+data["e-mail"]+'"/>' +
      '<label for="iva">Partita Iva</label>' +
      '<input type="text" class="input" id="iva" value="'+data["partita_iva"]+'"/>';
	  $("#Info").html(html_code);
  });

  //Orario
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=fascia&param="+param, function(data) {
    var html_code = "";
    html_code += '<table>'+
    '<thead>'+
      '<tr>'+
        '<th>Ora Inizio</th>'+
        '<th>Ora Fine</th>'+
        '<th>Numero Clienti</th>'+
      '</tr>'+
    '</thead>'+
    '<tbody id="contenuto">'+
    '</tbody>'+
    '</table>';
    $("#Orario").html(html_code);

    for(var i = 0; i < data.length; i++) {
      html_code = "";
      html_code += '<tr>'+
        '<td>'+data[i]["ora_inizio"].substring(0, 5)+'</td>'+
        '<td>'+data[i]["ora_fine"].substring(0, 5)+'</td>'+
        '<td>'+data[i]["n_clienti"]+'</td>'+
      '</tr>';
      $("tbody#contenuto").append(html_code);
    }
  });
}

function send(id){
	var myid = id;
	var qt = $("input#"+id+"").val();
	$.getJSON("../php/pagina_negozio.php?request=send&id="+id+"&qt="+qt, function(data){
		alert("Aggiunto al carrello");
	});
}

function send_rec(){
	var url = new URL(document.URL);
	var param = url.searchParams.get("user");
	var punt;
	var rec;
	punt = document.getElementById("stelle");
	rec = document.getElementById("recens");
	$.getJSON("../php/pagina_negozio.php?request=send_rec&id="+param+"&stelle="+punt+"&recens="+rec, function(data){
		alert("Recensione aggiunta!");
	});
}
