$(document).ready(function() {
  $( "#datepicker" ).datepicker();

  $(".hours").timepicker({
    onHourShow: OnHourShowCallback,
    showMinutes: false
  });

  $(".minutes").timepicker({
    showHours: false
  });

  $("#datepicker").on("change",function(){
    $("#tmp .hours").timepicker('option', {
      onHourShow: OnHourShowCallback2
    });
  });

  $("#tmp .hours").on("click", function() {
    $("#tmp .minutes").focus();
  });

  function OnHourShowCallback(hour) {
    array = new Array(2,3,5);
    if (jQuery.inArray(hour, array) != -1) {
        return false; // not valid
    }
    return true; // valid
  }

  function OnHourShowCallback2(hour) {
    array = new Array(7,9,10);
    if (jQuery.inArray(hour, array) != -1) {
        return false; // not valid
    }
    return true; // valid
  }
});
