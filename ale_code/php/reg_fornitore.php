<?php
if(isset($_GET["sent"])){
	$errors = "";
	$insertError = "";


	if(!isset($_GET["nome"])){
		$errors .= "Nome è obbligatorio! <br/>";
	}

	if(!isset($_GET["cognome"])){
		$errors .= "Cognome è obbligatorio! <br/>";
	}

	if(!isset($_GET["email"]) || !filter_var($_GET["email"], FILTER_VALIDATE_EMAIL)){
		$errors .= "Email è obbligatoria e deve essere valida <br/>";
	}

	if(!isset($_GET["user"])){
		$errors .= "Username è obbligatorio! <br/>";
	}

	if(!isset($_GET["partitaiva"])){
		$errors .= "Partita iva è obbligatoria! <br/>";
	}

	if(!isset($_GET["pwd"])){
		$errors .= "Password obbligatoria <br/>";
	}

	if(!isset($_GET["verpwd"])){
		$errors .= "Inserire password per verifica! <br/>";
	}

	if($_GET["pwd"] != $_GET["verpwd"]){
		$errors .= "Le due password inserite sono diverse! <br/>";
	}

	if(strlen($errors) == 0){

		$servername = "ftp.entrega.altervista.org";
		$username = "entrega";
		$password = "AleFiloLuca1";
		$dbname = "my_entrega";

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$img = addslashes($_GET["image"]);
		$nome = $_GET["nome"];
		$cognome = $_GET["cognome"];
		$email = $_GET["email"];
		$user = $_GET["user"];
		$pwd = $_GET["pwd"];
		$partitaiva = $_GET["partitaiva"];

		$stmt = $conn->prepare("SELECT * FROM `account` WHERE `user` = ?");
		$stmt->bind_param("s", $user);
		$result = $stmt->get_result();
		$num_of_rows = $result->num_rows;
		if($num_of_rows > 0){
			$stmt->close();
			$errors ="Utente già esistente! </br>";
		}else{
			$stmt = $conn->prepare("SELECT * FROM `account` WHERE `partita_iva` = ?");
			$stmt->bind_param("s", $partitaiva);
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				$errors ="P.Iva già esistente! </br>";
			}else{
			
			$stmt = $conn->prepare("INSERT INTO `account`(`nome`, `cognome`, `e-mail`, `user`, `pwd`, `partita_iva`, `image`) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("sssssss", $nome, $cognome, $email, $user, $pwd, $partitaiva, $img);
			$isInserted = $stmt->execute();
			if(!$isInserted){
				$insertError = $stmt->error;
			}else{
			$stmt = $conn->prepare("INSERT INTO `fornitore`(`user`, `accettato`) VALUES (?, '0')");
			$stmt->bind_param("s", $user);
			$isInserted = $stmt->execute();
			if(!$isInserted){
				$insertError = $stmt->error;
			}else {
				$stmt->close();
				header("Location:../html/login.html");
			}}
		}
	}
	}
}
?>


<!doctype html>
<html lang="it" dir="ltr">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/style.css">
	<script src="../javascript/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="../javascript/reg_fornitor.js" type="text/javascript"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Registrazione Fornitore</title>
</head>
<header>
	<img src="../img/logo.png" alt="logo"> </img>
</header>
<body>
	<div class="row">
		<div class="col-12 col-md-4 offset-md-4">
			<?php
			if(isset($_GET["sent"])){
				if(strlen($errors) == 0 and $isInserted)
				{
					?>
					<div class="alert alert-success alert-php" role="alert">
						Inserimento avvenuto correttamente!
					</div>
					<?php
				}
				else{
					?>
					<div class="alert alert-danger alert-php" role="alert">
						Errore durante l'inserimento!
						<p><?=$errors?><?=$insertError?></p>
					</div>
					<?php
				}
			}
			?>
			<div class="alert alert-danger alert-js" role="alert" style="display: None">
				Dati inseriti non corretti
				<p></p>
			</div>
			<form id="insertform" method="get" action="#">
				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" name="nome" class="form-control" id="inputNome" placeholder="Inserisci Nome">
				</div>
				<div class="form-group">
					<label for="inputCognome">Cognome</label>
					<input type="text" name="cognome" class="form-control" id="inputCognome" placeholder="Inserisci Cognome">
				</div>
				<div class="form-group">
					<label for="inputEmail">Indirizzo Email</label>
					<input type="email" name="email"  class="form-control" id="inputEmail" placeholder="Inserisci Email">
				</div>
				<div class="form-group">
					<label for="inputUser">Username</label>
					<input type="text" name="user" class="form-control" id="inputUsername" placeholder="Inserisci Username">
				</div>
				<div class="form-group">
					<label for="inputPartitaiva">Partita Iva</label>
					<input type="text" name="partitaiva" class="form-control" id="inputPartitaiva" placeholder="Inserisci Partita Iva">
				</div>
				<div class="form-group">
					<label for="inputImg">Immagine</label>
					<input type="file" name="img"  class="form-control" id="inputImg" accept="img/png , img/jpeg" placeholder="Inserisci Immagine">
				</div>
				<div class="form-group">
					<label for="inputPwd">Password</label>
					<input type="password" name="pwd" class="form-control" id="inputPwd" placeholder="Inserisci Password">
				</div>
				<div class="form-group">
					<label for="inputVerPassword">Verifica Password</label>
					<input type="password" name="verpwd" class="form-control" id="inputVerPassword" placeholder="Conferma Password">
				</div>
				<input type="hidden" name="sent" value="true" />
				<button type="submit" class="btn btn-primary">Aggiungi Fornitore</button>
			</form>
		</div>
	</div>
</body>
</html>
<?php
if(isset($_GET["sent"])){
	$errors = "";
	$insertError = "";


	if(!isset($_GET["nome"])){
		$errors .= "Nome è obbligatorio! <br/>";
	}

	if(!isset($_GET["cognome"])){
		$errors .= "Cognome è obbligatorio! <br/>";
	}

	if(!isset($_GET["email"]) || !filter_var($_GET["email"], FILTER_VALIDATE_EMAIL)){
		$errors .= "Email è obbligatoria e deve essere valida <br/>";
	}

	if(!isset($_GET["user"])){
		$errors .= "Username è obbligatorio! <br/>";
	}

	if(!isset($_GET["partitaiva"])){
		$errors .= "Partita iva è obbligatoria! <br/>";
	}

	if(!isset($_GET["pwd"])){
		$errors .= "Password obbligatoria <br/>";
	}

	if(!isset($_GET["verpwd"])){
		$errors .= "Inserire password per verifica! <br/>";
	}

	if($_GET["pwd"] != $_GET["verpwd"]){
		$errors .= "Le due password inserite sono diverse! <br/>";
	}

	if(strlen($errors) == 0){

		$servername = "ftp.entrega.altervista.org";
		$username = "entrega";
		$password = "AleFiloLuca1";
		$dbname = "my_entrega";

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		$img = addslashes($_GET["image"]);
		$nome = $_GET["nome"];
		$cognome = $_GET["cognome"];
		$email = $_GET["email"];
		$user = $_GET["user"];
		$pwd = $_GET["pwd"];
		$partitaiva = $_GET["partitaiva"];

		$stmt = $conn->prepare("SELECT * FROM `account` WHERE `user` = ?");
		$stmt->bind_param("s", $user);
		$result = $stmt->get_result();
		$num_of_rows = $result->num_rows;
		if($num_of_rows > 0){
			$stmt->close();
			$errors ="Utente già esistente! </br>";
		}else{
			$stmt = $conn->prepare("SELECT * FROM `account` WHERE `partita_iva` = ?");
			$stmt->bind_param("s", $partitaiva);
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				$errors ="P.Iva già esistente! </br>";
			}else{
			
			$stmt = $conn->prepare("INSERT INTO `account`(`nome`, `cognome`, `e-mail`, `user`, `pwd`, `partita_iva`, `image`) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("sssssss", $nome, $cognome, $email, $user, $pwd, $partitaiva, $img);
			$isInserted = $stmt->execute();
			if(!$isInserted){
				$insertError = $stmt->error;
			}else{
			$stmt = $conn->prepare("INSERT INTO `fornitore`(`user`, `accettato`) VALUES (?, '0')");
			$stmt->bind_param("s", $user);
			$isInserted = $stmt->execute();
			if(!$isInserted){
				$insertError = $stmt->error;
			}else {
				$stmt->close();
				header("Location:../html/login.html");
			}}
		}
	}
	}
}
?>


<!doctype html>
<html lang="it" dir="ltr">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<script src="../javascript/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="../javascript/reg_fornitor.js" type="text/javascript"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Registrazione Fornitore</title>
</head>
<header>
	<nav id="icons">
		<div id="right_icons">
		  <a href="../html/login.html" title="LogIn">
		  <span class="icon fas fa-sign-in-alt fa-5x"></span>
		  </a>
			</div>
	</nav>
	<img src="../img/logo.png" alt="logo"> </img>
	
</header>
<body>
	<div class="row">
		<div class="col-12 col-md-4 offset-md-4">
			<?php
			if(isset($_GET["sent"])){
				if(strlen($errors) == 0 and $isInserted)
				{
					?>
					<div class="alert alert-success alert-php" role="alert">
						Inserimento avvenuto correttamente!
					</div>
					<?php
				}
				else{
					?>
					<div class="alert alert-danger alert-php" role="alert">
						Errore durante l'inserimento!
						<p><?=$errors?><?=$insertError?></p>
					</div>
					<?php
				}
			}
			?>
			<div class="alert alert-danger alert-js" role="alert" style="display: None">
				Dati inseriti non corretti
				<p></p>
			</div>
			<form id="insertform" method="get" action="#">
				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" name="nome" class="form-control" id="inputNome" placeholder="Inserisci Nome">
				</div>
				<div class="form-group">
					<label for="inputCognome">Cognome</label>
					<input type="text" name="cognome" class="form-control" id="inputCognome" placeholder="Inserisci Cognome">
				</div>
				<div class="form-group">
					<label for="inputEmail">Indirizzo Email</label>
					<input type="email" name="email"  class="form-control" id="inputEmail" placeholder="Inserisci Email">
				</div>
				<div class="form-group">
					<label for="inputUser">Username</label>
					<input type="text" name="user" class="form-control" id="inputUsername" placeholder="Inserisci Username">
				</div>
				<div class="form-group">
					<label for="inputPartitaiva">Partita Iva</label>
					<input type="text" name="partitaiva" class="form-control" id="inputPartitaiva" placeholder="Inserisci Partita Iva">
				</div>
				<div class="form-group">
					<label for="inputImg">Immagine</label>
					<input type="file" name="img"  class="form-control" id="inputImg" accept="img/png , img/jpeg" placeholder="Inserisci Immagine">
				</div>
				<div class="form-group">
					<label for="inputPwd">Password</label>
					<input type="password" name="pwd" class="form-control" id="inputPwd" placeholder="Inserisci Password">
				</div>
				<div class="form-group">
					<label for="inputVerPassword">Verifica Password</label>
					<input type="password" name="verpwd" class="form-control" id="inputVerPassword" placeholder="Conferma Password">
				</div>
				<input type="hidden" name="sent" value="true" />
				<button type="submit" class="btn btn-primary">Aggiungi Fornitore</button>
			</form>
		</div>
	</div>
</body>
</html>
