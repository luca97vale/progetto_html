$.getJSON("../php/pagina_negozio.php?request=session", function(data) {
  if(data[0] == 0){
    window.location.replace("../../ale/html/login.html");
  }
});
function openBar(evt, elem) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(elem).style.display = "block";
  evt.currentTarget.className += " active";

};
$(document).ready(function(){
  ready();
});
function ready(){
  openBar(window.event,'Menu')
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=forn&negozio="+param, function(data) {
    var html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code +='<label>'+data[i]["nome"]+'</label>'
      html_code += '<li><label >'+data[i]["descrizione"]+'</label>';
      html_code += '<input type="number" disabled id="value'+data[i]["id"]+'" name="prezzo" min="1.00" max="100.00" step="0.10" value='+data[i]["prezzo"]+'>€ <button type="button" onclick="send('+data[i]["id"]+
					')"> Aggiungi al carrello </button>  <input type="text" name="'+data[i]["id"]+'" id="'+data[i]["id"]+'" placeholder="Inserisci quantità"> </input> </li> </ul>';
    }
    $("#Menu").html(html_code);
  });
  $.getJSON("../php/pagina_negozio.php?request=get", function(data) {
    var html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="container">';
      html_code += '<p><span>'+data[i]["userUtente"]+'</span> </p>';
      var stelle = data[i]["stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
    $("#Recensioni").html(html_code);
  });
}
function send(id){
	var myid = id;
	var qt = $("input#"+id+"").val();
	$.getJSON("../php/pagina_negozio.php?request=send&id="+id+"&qt="+qt, function(data){
		console.log("ID = " + id + "	quantità = " + qt);
		alert("Aggiunto al carrello");
	});
}