$(document).ready(function(){
	document.getElementById("alert-js").style.display="none";
	$("#submit").click(function(){
		var user = $("input#inputUser").val();
		var pwd = $("input#inputPassword").val();
		$.getJSON("../php/login.php?request=login&usr="+user+"&pwd="+pwd, function(data) {
			var html_code = "";
			for(var i = 0; i < data.length; i++){
				$.getJSON("../php/login.php?request=user&usr="+user, function(data) {
					if(data.length>0){
						window.location.replace("../html/home_utente.html");
					}
				});
				$.getJSON("../php/login.php?request=admin&usr="+user, function(data) {
					if(data.length>0){
						window.location.replace("../../filo/html/initial_Admin_Page.html");
					}
				});
				$.getJSON("../php/login.php?request=forn&usr="+user, function(data) {
					if(data.length>0){
						window.location.replace("../../luca/html/fornitore.html");
					}
				});
			}
		});
		$(".alert-php").hide();
		event.preventDefault();
		errors = "";

		var user = $("input#inputUser").val();
		var pwd = $("input#inputPassword").val();

		if(user == ''){
			errors += "<strong> ATTENZIONE </strong> Inserire Utente!<br/>";
			document.f_log.pwd.value = "";
		}

		if(pwd == ''){
			errors += "<strong> ATTENZIONE </strong> Inserire Password!<br/>";
			document.f_log.user.value = "";
		}
		if(pwd != '' && user != ''){
			$.getJSON("../php/login.php?request=login&usr="+user+"&pwd="+pwd, function(data) {
				if(data.length != 1){
					document.f_log.user.value = "";
					document.f_log.pwd.value = "";
					errors = "<strong> ATTENZIONE </strong> Username e/o password non corretti! <br/>";
					document.getElementById("alert-js").style.display="block";
					$("div#alert-js").html(errors);
					errors = "";
				}
			});
		}
		if(errors.length > 0)
		{
			document.getElementById("alert-js").style.display="block";
			$("div#alert-js").html(errors);
			errors = "";
		}
		else
		{
			$(this).parent().submit();
		}
	});
});
