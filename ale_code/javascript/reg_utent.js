function validateEmail(email) 
{
	var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
	return regex.test(email);
}

function hasWhiteSpace(s) {
	console.log(s.indexOf(' ') >= 0);
	return s.indexOf(' ') >= 0;
}

$(document).ready(function(){
	$("form button").click(function(){
		
		$(".alert-php").hide();
		event.preventDefault();
		errors = "";
		
		var nome = $("input#inputNome").val();
		var cognome = $("input#inputCognome").val();
		var email = $("input#inputEmail").val();
		var nomeUtente = $("input#inputUsername").val();
		var pwd = $("input#inputPwd").val();
		var verpwd = $("input#inputVerPassword").val();
		
		if(nome == null){
			errors += "Nome è obbligatorio!<br/>";
		}

		if(cognome == null){
			errors += "Cognome è obbligatorio!<br/>";
		}
		
		if(email == null || !validateEmail(email)){
			errors += "Email è obbligatoria e deve essere valida<br/>";
		}
		
		if(nomeUtente == null || hasWhiteSpace(nomeUtente)){
			errors += "Username è obbligatorio e non può contenere spazi! <br/>";
		}
	
		if(pwd == null){
			errors += "Password obbligatoria <br/>";
		}
		
		if(verpwd == null){
			errors += "Inserire password per verifica! <br/>";
		}
		
		if(pwd != verpwd){
			errors += "Le due password inserite sono diverse! <br/>";
		}

		if(errors.length > 0)
		{
			var nome = $("div.alert-js p").html(errors);
			$("div.alert-js").show();
		}
		else
		{
			$(this).parent().submit();
		}

	});
});