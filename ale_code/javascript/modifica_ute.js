function validateEmail(email)
{
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return regex.test(email);
}

$(document).ready(function(){ 
	html_code = "";
	document.getElementById("action").style.display="none";
	document.getElementById("succ").style.display="none";
	document.getElementById("mex").style.display="none";
	$.getJSON("../php/modifica_ute.php?request=current", function(data) {
		var user = data[0]['user'];
		var nome = data[0]['nome'];
		var cognome = data[0]['cognome'];
		var email = data[0]['e-mail'];
		var pwd = data[0]['pwd'];
		html_code = '<label>Nome</label> <label id="current">'+'<input type="text" disabled value="'+ nome + '"> </input>' + 
					'<label>Cognome</label> <label id="current">'+'<input type="text" disabled value="'+ cognome + '"> </input>' +
					'<label>E-Mail</label> <label id="current">'+ '<input type="text" disabled value="'+ email + '"> </input>' +
					'<label>User</label> <label id="current">'+ '<input type="text" disabled value="'+ user + '"> </input>';
		$("form#prev").before(html_code);
	});
	$("#update").click(function(){
		var newemail = $("input#inputEmail").val();
		var newpwd = $("input#inputPwd").val();
		var verpwd = $("input#inputVerpwd").val();

		$.getJSON("../php/modifica_ute.php?request=update&newpwd="+newpwd+"&newemail="+newemail+"&verpwd="+verpwd, function(mex) {
				$.getJSON("../php/modifica_ute.php?request=current", function(ris) {
					html_code = '<label>Nome</label> <label id="current">'+'<input type="text" disabled value="'+ ris[0]['nome'] + '"> </input>' + 
					'<label>Cognome</label> <label id="current">'+'<input type="text" disabled value="'+ ris[0]['cognome'] + '"> </input>' +
					'<label>E-Mail</label> <label id="current">'+ '<input type="text" disabled value="'+ ris[0]['email'] + '"> </input>' +
					'<label>User</label> <label id="current">'+ '<input type="text" disabled value="'+ ris[0]['user'] + '"> </input>';
				});
			$("form#succ").html(html_code);
			$("div#mex p").html(mex);
			document.getElementById("mex").style.display="block";
			document.getElementById("succ").style.display="block";
			document.getElementById("action").style.display="none";
		});
	});
	$("#show").click(function(){
		document.getElementById("prev").style.display="none";
		document.getElementById("action").style.display="block";
	});
});