$.getJSON("../php/session.php?request=user", function(data) {
  if(data == 0){
    window.location.href = "../../ale/html/login.html?&err=-1";
  }
});

$(document).ready(function() {

  $("#info").css("background-color", "#fd9d4e");

  $("#info").click(function() {
    $("#foods").css("display", "none");
    $("#details").css("display", "block");
    $("#info").css("background-color", "#fd9d4e");
    $("#menu").css("background-color", "#fd6a02");
  });

  $("#menu").click(function() {
    $("#details").css("display", "none");
    $("#foods").css("display", "block");
    $("#info").css("background-color", "#fd6a02");
    $("#menu").css("background-color", "#fd9d4e");
  });

  $.getJSON("../php/vendor_details.php?request=getDetails", function(data) {
    if (data["image"] == null) {
      var image = "../base.jpg";
    } else {
      var image = data["image"];
    }
    $("#details img").attr("src", image);
    $("h1").text(data["nome_negozio"]);
    $("input#name").val(data["nome"]);
    $("input#surname").val(data["cognome"]);
    $("input#p_iva").val(data["p_iva"]);
  });

  $.getJSON("../php/vendor_details.php?request=getMenu", function(data) {
  });
});
