$.getJSON("../php/session.php?request=admin", function(data) {
  if(data == 0){
    window.location.href = "../../ale/html/login.html?&err=-1";
  }
});

function validateEmail(email) {
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return regex.test(email);
}

function edit(name, surname, email, pwd) {
  $.post("../php/edit_admin.php?request=edit", {name:name, surname:surname, email:email, pwd:pwd}, function(data) {
  }), "text";
}

$(document).ready(function() {
  var details;

  $.getJSON("../php/edit_admin.php?request=getDetails", function(data) {
    details = data;

    if (data["image"] == null) {
      var image = "../base.jpg";
    } else {
      var image = data["image"];
    }
    $("#details img").attr("src", image);
    $("input#user").val(data["user"]);
    $("input#editName").val(data["nome"]);
    $("input#editSurname").val(data["cognome"]);
    $("input#editEmail").val(data["e-mail"]);
  });

  $("form").on("click", "input#save", function() {
    var html_code = "";

    var name = details["nome"];
    var surname = details["cognome"];
    var email = details["e-mail"];
    var pwd = details["pwd"];

    var newName = $("#editName").val();
    var newSurname = $("#editSurname").val();
    var newEmail = $("#editEmail").val();
    var newPwd = $("#editPwd").val();
    var confirmPwd = $("#confirmPwd").val();
    var newImg = $("#editImg")[0].files[0];

    if (newName != "" && newName != name) {
      name = newName;
      html_code += "<p>Il nome è stato cambiato</p>";
    }

    if (newSurname != "" && newSurname != surname) {
      surname = newSurname;
      html_code += "<p>Il cognome è stato cambiato</p>";
    }

    if (newPwd != "" && newPwd == confirmPwd) {
      pwd = newPwd;
      html_code += "<p>La password è stata cambiata</p>";
    }

    if (newEmail != "" && validateEmail(newEmail) && newEmail != email) {
      $.getJSON("../php/edit_admin.php?request=email", {email:email}, function(data) {
        if(data == 0) {
          email = newEmail;
          html_code += "<p>La email è stata cambiata</p>";
        }
        edit(name, surname, email, pwd);
      });
    } else {
      edit(name, surname, email, pwd);
    }

    if (typeof newImg != 'undefined') {
      html_code += "<p>L'immagine del profilo è stata cambiata</p>";
      reader = new FileReader();
      reader.onload = function(evt) {
        var image = evt.target.result;
        $.post("../php/edit_admin.php?request=editImage", {image:image}, function(data) {
          $(".result").html(html_code);
          $("#details img").attr("src", image);
        }), "text";
      };
      reader.readAsDataURL(new Blob([newImg]));
    } else {
      $(".result").html(html_code);
    }
  });
});
