$.getJSON("../php/session.php?request=user", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});

function searchbar() {
  if($("#search").val().length > 0) {
    $("li h2").each(function() {
      if(!$(this).text().toUpperCase().startsWith($("#search").val().toUpperCase())){
        $(this).parent().parent().css("display","none");
      } else {
        $(this).parent().parent().css("display","block");
      }
    });
  } else {
    $("li").each(function(index) {
      $(this).css("display","block");
    });
  }
}

$(document).ready(function() {
  var order = new Array();

  $("form#check-out input").click(function() {
    if ($.trim($("table").html())) {
      window.open("../html/check-out.html", "_self");
    }
  });

  $('header').on("keyup", "#search", searchbar);

  $("ul.content").on("click", ".fa-trash-alt", function() {
    var nome_negozio = $(this).parent().parent().parent().parent().parent().prev().text();
    var prodotto = $(this).parent().parent().prev().prev().prev().text();
    var id_ordine;
    var id_prodotto;
    $.each(order, function(index, element) {
      if (element["nome_negozio"] === nome_negozio && element["nome"] === prodotto) {
        id_ordine = element["id_ordine"];
        id_prodotto = element["id_prodotto"];
        return false;
      }
    });
    $.getJSON("../php/cart.php?request=removeItem",{id_prodotto:id_prodotto, id_ordine:id_ordine}, function(data) {
      var prezzo = parseFloat($("h2:contains("+nome_negozio+") + table tbody td.costo").html());
      var totale = parseFloat($("p#sum_total span").html());
      $("p#sum_total span").html((totale - prezzo).toFixed(2)+"€");
      $("td:contains('"+prodotto+"')").parent().remove();
      if (!$.trim($("h2:contains("+nome_negozio+") + table tbody").html())) {
        $("h2:contains("+nome_negozio+")").parent().parent().remove();
      }
    })
    .fail(function(data) {
    	alert("Something went wrong, contact the admin to solve it");
		});
  });

  $.getJSON("../php/cart.php?request=loadCart", function(data) {
    var prec_fornitore = "";
    order = data;

    for(var i = 0; i < data.length; i++){
      var html_code = "";
      var new_fornitore = data[i]["user_fornitore"];
      if (prec_fornitore !== new_fornitore) {
        html_code += '<li>'+
            '<article class="shop">'+
              '<h2>'+data[i]["nome_negozio"]+'</h2>'+
              '<table>'+
                '<thead class="header">'+
                  '<tr>'+
                    '<th>Pietanza</th>'+
                    '<th>Qtà</th>'+
                    '<th>Costo</th>'+
                    '<th></th>'+
                  '</tr>'+
                '</thead>'+
                '<tbody class="dishes">'+
                '</tbody>'+
              '</table>'+
            '</article>'+
          '</li>';

        $("ul.content").append(html_code);
      }

      html_code = "";
      html_code += '<tr>'+
            '<td>'+data[i]["nome"]+'</td>'+
            '<td>'+data[i]["quantita"]+'</td>'+
            '<td class="costo">'+(data[i]["Prezzo"]*parseFloat(data[i]["quantita"])).toFixed(2)+'€</td>'+
            '<td><a href="#" title="Remove from cart">'+
              '<span class="icon fas fa-trash-alt"></span>'+
            '</a></td>'+
          '</tr>';

      $("h2:contains("+data[i]["nome_negozio"]+") + table tbody").append(html_code);

      prec_fornitore = new_fornitore;
    }

    var sum_total = 0;
    $("td.costo").each(function(){
      sum_total += parseFloat($(this).html());
    });

    $("p#sum_total span").html(sum_total.toFixed(2)+"€");

    if ($("h2").length > 1) {
      $("article.shop tbody tr:last-child").css("border-bottom", "2px double black");
    }
  });
});
