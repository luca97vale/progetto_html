$.getJSON("../php/session.php?request=user", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});

var shops = new Array();
var hours = new Map();

function OnDateShowCallback(date) {
  return [!(date.getDay() == 6 || date.getDay() == 0), ""];
}

function OnHourShowCallback(hour, arr) {
  return ((jQuery.inArray(hour, arr) != -1) && hour >= 6 && hour <= 18);
}

function editAvailableHours() {
  $("h2.negozio").each(function() {
    var date = $(this).next().find(".datepicker").val();
    var nome_negozio = $(this).text();
    var fornitore = shops[nome_negozio];
    var size = 0;
    if (typeof hours.get(nome_negozio) != 'undefined') {
      size = hours.get(nome_negozio).length;
    }

    if (date != '' && date.length > 0) {
      $.getJSON("../php/check_out.php?request=getSlots", {date:date, fornitore:fornitore}, function(data) {
        if (data.length == 0) {
          hours.set(nome_negozio, new Array());
        } else {
          $.each(data, function(index, element) {
            if (!hours.has(element["nome_negozio"])) {
              hours.set(element["nome_negozio"], new Array());
            }
            if (hours.get(element["nome_negozio"]).indexOf(element["ora"]) == -1) {
              hours.get(element["nome_negozio"]).push(element["ora"]);
            }
          });
        }

        if ($("h2:contains("+nome_negozio+")").next().find("input.hours").attr("disabled") == "disabled") {
          $("h2:contains("+nome_negozio+")").next().find("input.hours").removeAttr("disabled");
          $("h2:contains("+nome_negozio+")").next().find("input.minutes").removeAttr("disabled");

          $("h2:contains("+nome_negozio+")").next().find(".minutes").timepicker({
            showHours: false,
            defaultTime: ""
          });

          $("h2:contains("+nome_negozio+")").next().find(".hours").timepicker({
            showMinutes: false,
            defaultTime: "",
            onHourShow: function(hour) {
              return OnHourShowCallback(hour, hours.get(nome_negozio));
            }
          });
        } else if (size != hours.get(nome_negozio).length) {
          $("h2:contains("+nome_negozio+")").next().find(".hours").blur();
          setTimeout(function() {
            $("h2:contains("+nome_negozio+")").next().find(".hours").focus();
          }, 500);
          $("h2:contains("+nome_negozio+")").next().find(".hours").timepicker('option', {
            onHourShow: function(hour) {
              return OnHourShowCallback(hour, hours.get(nome_negozio));
            }
          });
        }
      });
    } else {
      $("h2:contains("+nome_negozio+")").next().find(".hours").attr("disabled", "disabled");
      $("h2:contains("+nome_negozio+")").next().find(".minutes").attr("disabled", "disabled");
    }
  });
}

$(document).ready(function() {

  setInterval(editAvailableHours, 5000);

  $(document).click(function() {
    $("#error").fadeOut();
  });

  $.getJSON("../php/check_out.php?request=loadShops", function(data) {
    var prec_fornitore = "";

    for(var i = 0; i < data.length; i++){
      var html_code = "";
      shops[data[i]["nome_negozio"]] = data[i]["user_fornitore"];
      html_code += '<li>'+
        '<article class="shop">'+
          '<h2 class=negozio>'+data[i]["nome_negozio"]+'</h2>'+
          '<table class="picker">'+
          '</table>'+
        '</article>'+
      '</li>';

      $("ul.content").append(html_code);

      html_code = "";
      html_code += '<tbody>'+
          '<tr>'+
            '<th>Data</th>'+
            '<td>'+
              '<label for="date" hidden>Data</label>'+
              '<input type="text" class="datepicker" readonly="true">'+
            '</td>'+
          '</tr>'+
          '<tr>'+
            '<th>Orario</th>'+
            '<td>'+
              '<label for="hours" hidden>Ore</label>'+
              '<input type="text" class="timepicker hours" readonly="true">'+
              '<p id="time-separator">:</p>'+
              '<label for="minutes" hidden>Minuti</label>'+
              '<input type="text" class="timepicker minutes" readonly="true">'+
            '</td>'+
          '</tr>'+
          '<tr>'+
            '<th>Metodo di pagamento</th>'+
            '<td>'+
              '<label for="payment" hidden>Scegli il metodo di pagamento</label>'+
              '<div class="select">'+
                '<select class="payment" name="Payment methods">'+
                  '<option value="card">Carta di credito</option>'+
                  '<option value="Cash">Contanti</option>'+
                '</select>'+
              '</div>'+
            '</td>'+
          '</tr>'+
          '<tr>'+
            '<th>Luogo di consegna</th>'+
            '<td>'+
              '<label for="delivery_place" hidden>Scegli il luogo di consegna</label>'+
              '<div class="select">'+
                '<select class="delivery_place" name="Place">'+
                  '<option value="Ground">Entrata Piano Terra</option>'+
                  '<option value="Floor1">Entrata Primo Piano</option>'+
                '</select>'+
              '</div>'+
            '</td>'+
          '</tr>'+
        '</tbody>';
      $("h2:contains("+data[i]["nome_negozio"]+") + table").append(html_code);

      $("h2:contains("+data[i]["nome_negozio"]+")").next().find(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        defaultDate: "today",
        minDate: "today",
        beforeShowDay: OnDateShowCallback
      });

      $("h2:contains("+data[i]["nome_negozio"]+")").next().find(".hours").attr("disabled", "disabled");
      $("h2:contains("+data[i]["nome_negozio"]+")").next().find(".minutes").attr("disabled", "disabled");
    }
  });

  $(document).on("keydown", "input[type=text]", false);
  $(document).on("keydown", "input[type=text]", false);

  $(document).on("change", "input.datepicker", function() {
    editAvailableHours();
  });

  $("input#order").click(function() {
    var error = 0;
    $("input.hours").each(function() {
      if (!$(this).val() || !$(this).next().next().next().val()) {
        $("p#error").text("Error: Devi selezionare un orario per ogni negozio!");
        error = 1;
        return false;
      }
    });
    if (!error) {
      $("input.hours").each(function() {
        var hour = $(this).val();
        var minutes = $(this).next().next().next().val();
        var date = $(this).parent().parent().prev().find("input.datepicker").val();
        var nome_negozio = $(this).parent().parent().parent().parent().prev().text();
        var fornitore = shops[nome_negozio];

        var countClienti;
        $.getJSON("../php/check_out.php?request=isFull", {date:date, fornitore:fornitore, ora:hour}, function(data) {
          countClienti = data;
          if (countClienti > 0) {
            var time = hour + ":" + minutes;
            var pagamento = $("h2:contains("+nome_negozio+") + table select.payment option:selected").text();
            var consegna = $("h2:contains("+nome_negozio+") + table select.delivery_place option:selected").text();
            $.post('../php/check_out.php?request=insert', {fornitore:fornitore, data:date, time:time, pagamento:pagamento, consegna:consegna}, function(data) {
              if (data == 0) {
                $("p#error").text("Error: Errore durante il salvataggio. Contatta l'admin.");
              } else {
                window.location.replace("../../ale/html/home_utente.html");
              }
            })
            .fail(function(data) {
              $("p#error").text("Error: Errore durante il salvataggio. Contatta l'admin.");
            }, 'text');
          } else {
            $("p#error").text("Error: Il fornitore "+fornitore+" non riesce a servire altri clienti nella fascia oraria selezionata.");
          }
        });
      });
    }
  });
});
