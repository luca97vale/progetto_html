<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

$conn = setConnection();

$stmt = $conn->prepare("SELECT image FROM account a WHERE user = ?");
$stmt->bind_param("s", $_SESSION["user"]);

$stmt->execute();

$result = $stmt->get_result();

$output = $result->fetch_assoc();

$stmt->close();
print json_encode($output);

?>
