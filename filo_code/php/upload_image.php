<?php
header('Content-Type: application/json');
include "connection.php";

$conn = setConnection();

$image = addslashes($_POST["image"]);

$stmt = $conn->prepare("UPDATE account SET image = ? WHERE user = 'Filo'");
$stmt->bind_param("s", $image);
$stmt->execute();

if ($stmt->affected_rows > 0) {
	$response_array['status'] = 'success';
} else {
	$response_array['status'] = 'error';
}

$stmt->close();
print json_encode($response_array);
?>
